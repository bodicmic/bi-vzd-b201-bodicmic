# Knight's Tour projekt

autor: Michal Bodický (bodicmic)


Tato složka obsahuje soubory kt_funkce.py a test_kt.py, sloužící k vyřešení uživatelem zadaného problému pro Knight's Tour.
Dále soubor bodicmic.pdf představující Knight's Tour a výzkum s řešičem spojený v detailu a requirements.txt zachycující
použité prostředí při vývoji kódu.

## kt_funkce.py

V souboru jsou definovány následující funkce: start, resitelne, hledac, vyres, zobraz a spust.
Dále je zaznamenána proměnná tahy, značící možné pohyby jezdce po šachovnici.

Jednotlivé funkce tvoří operace pro zadání, řešení a vizualizaci problému, jejichž správné spuštění je obstaráno funkcí spust,
jejž je spuštěna zavoláním programu. 

Funkce start nechá uživatele zadat jednotlivé parametry problému, funkce resitelne vrátí zda-li úlohu má smysl řešit a dále
funkce hledac a vyres úlohu společně vyřeší do maticové formy. Nakonec funkce zobraz vykreslí daný output.

Program lze spustit přímo z příkazové řádky jeho zavoláním.

```bash
kt_funkce.py
```

Následně je uživatel dotázán na 4 parametry zadávající úlohu: počet řádků a slouců řešeného pole a pozici začátku cesty jako řádek a sloupec.


## test_kt.py

Testovací soubor provádějící unitární testy pro 3 funkce z kt_funkce.py. Testuje, zda-li funkce resitelne, vyres a hledac fungují správně.
Dále testuje, zda-li společný výstup těchto funkcí odpovídá správnému řešení problému.

Pro správné spuštění, je zapotřebí mít soubor ve stejném adresáři jako kt_funkce.py a stačí jej zavolal přes pytest příkaz.

```bash
pytest
```

## Prostředí

Pro zaručenou funkčnost programu doporučujeme použít následující verze knihoven Pythonu 3.9.5, je možné je naistalovat následovně:

```bash
pip install numpy==1.19.5, pandas==1.1.2, pytest==6.2.2, matplotlib==3.3.2
```

Ostatní knihovny nainstalované při vývoji jsou uvedeny v dokumentu requirements.txt.